import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";


function NewHat() {
    const navigate = useNavigate()
    const [locations, setLocations] = useState([])

    const [formData, setFormData] = useState({
        style_name: '',
        fabric: '',
        color: '',
        picture_url: '',
        location: '',
    })

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/locations')
        if (response.ok){
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(()=> {fetchData()}, [])

    const handleChange = event =>{
        setFormData({...formData, [event.target.name]: event.target.value})
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const url = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method:'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                style_name: '',
                fabric: '',
                color: '',
                picture_url: '',
                location: '',
            })
            navigate('/hats')
        }
    }

    return (
        <React.Fragment>
            <div className='row'>
            <div className='offset-3 col-6'>
                <div className="shadow p-4 mt-4">
                    <h1>New Hat!</h1>
                    <form id="create-hat-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="Style" required type='text' name="style_name" id="style_name" className="form-control" onChange={handleChange}></input>
                            <label htmlFor="style_name">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Fabric" required type='text' name="fabric" id="fabric" className="form-control" onChange={handleChange}></input>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Color" required type='text' name="color" id="color" className="form-control" onChange={handleChange}></input>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Picture Url" required type='text' name="picture_url" id="picture_url" className="form-control" onChange={handleChange}></input>
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select required name="location" id="location" className="form-select" onChange={handleChange}>
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option value={location.import_href} key={location.import_href}>{location.location_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
            </div>
        </React.Fragment>
    )
}

export default NewHat
