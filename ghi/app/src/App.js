import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './compenents/MainPage';
import Nav from './compenents/Nav';
import HatsList from './compenents/HatsList';
import NewHat from './compenents/NewHat';
import HatDetails from './compenents/HatDetails';

function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />}/>
          <Route path='hats'>
            <Route path='' element={<HatsList/>}/>
            <Route path='new' element={<NewHat/>}/>
            <Route path=':id' element={<HatDetails/>}/>
          </Route>

        </Routes>
    </BrowserRouter>
  );
}

export default App;
